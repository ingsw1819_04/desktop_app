package Window.Controller;

import Utilities.SearchProducts2;
import Window.Controller.Charts.AreaCharts;
import Window.Controller.Charts.BarCharts;
import Window.Controller.Charts.LineCharts;
import Window.Controller.Charts.PieCharts;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ScrollPane;
import org.controlsfx.control.textfield.CustomTextField;
import org.controlsfx.control.textfield.TextFields;

import java.util.List;

public class StatsViewController {

    //CODICE PER RICERCA2
    private static List<String> retrieveSearchedProducts = SearchProducts2.retrieveSearchedProducts();
    @FXML
    CustomTextField searchField;
    @FXML
    ScrollPane generalScrollPane, userScrollPane, productScrollPane, selectedProductScrollPane;
    @FXML
    PieChart sexChart, bestCategoryChart, regionChart;
    @FXML
    BarChart ageChart, categoryChart, categoryChart2, top10Products, top10Products2, subsLastWeek, subsLastMonth,
            subsLastYear, subsEver, bestClientOrderWeek, bestClientOrderMonth, bestClientOrderYear, bestClientOrderEver,
            bestClientCostWeek, bestClientCostMonth, bestClientCostYear, bestClientCostEver, bestCategoryChartWeek,
            bestCategoryChartMonth, bestCategoryChartYear, bestCategoryChartEver, selProdAvailChart;
    @FXML
    LineChart salesThroughTime, salesThroughTime2, subsLastWeek2, subsLastMonth2, subsLastYear2, subsEver2,
            selProdSalesChart, selProdPriceChart;
    @FXML
    AreaChart costLastWeek, costLastMonth, costLastYear, costEver, costLastWeek2, costLastMonth2, costLastYear2, costEver2;

    /**
     * Calls a method for auto-completing the search
     */
    public void retrieveProducts() {
        //CODICE PER RICERCA1
        //List<String> retrieveSearchedProducts = SearchProducts.retrieveSearchedProducts(searchField.getText());

        //CODICE PER RICERCA2
        TextFields.bindAutoCompletion(searchField, retrieveSearchedProducts);
    }

    /**
     * View the charts of the product you are looking for
     */
    public void selectedProductStats() {
        userScrollPane.setDisable(true);
        userScrollPane.setVisible(false);
        productScrollPane.setDisable(true);
        productScrollPane.setVisible(false);
        generalScrollPane.setDisable(true);
        generalScrollPane.setVisible(false);
        selectedProductScrollPane.setDisable(false);
        selectedProductScrollPane.setVisible(true);
        fillSelectedProductCharts(selProdSalesChart, selProdPriceChart, selProdAvailChart);
    }

    /**
     * Allows you to update the charts of the current tab
     */
    public void refreshCharts() {
        if (generalScrollPane.isVisible())
            viewGeneralScrollPane();
        else if (userScrollPane.isVisible())
            viewUserScrollPane();
        else if (productScrollPane.isVisible())
            viewProductScrollPane();
        else
            fillSelectedProductCharts(selProdSalesChart, selProdPriceChart, selProdAvailChart);
        //CODICE PER RICERCA2
        retrieveSearchedProducts = SearchProducts2.retrieveSearchedProducts();
    }

    /**
     * Fills the charts of the product you are looking for
     *
     * @param salesChart        is the sale chart of the product you are looking for
     * @param costChart         is the cost chart of the product you are looking for
     * @param availabilityChart is the availability chart of the product you are looking for
     */
    private void fillSelectedProductCharts(LineChart salesChart, LineChart costChart, BarChart availabilityChart) {
        String productName = searchField.getText();
        LineCharts.fillSelectedProductSalesTime(salesChart, productName);
        LineCharts.fillSelectedProductCostTime(costChart, productName);
        BarCharts.fillSelectedProductCostTime(availabilityChart, productName);
    }

    /**
     * Fill in the charts to be displayed in the general tab
     */
    private void fillGeneralCharts() {
        BarCharts.fillCategoryChart(categoryChart);
        BarCharts.filltop10ProductsChart(top10Products);
        LineCharts.fillSalesTimeChart(salesThroughTime);
        fillTimeGeneralCharts();
    }

    /**
     * Fill in the time charts to be displayed in the general tab
     */
    private void fillTimeGeneralCharts() {
        LineCharts.fillSubsWeekChart(subsLastWeek2);
        LineCharts.fillSubsMonthChart(subsLastMonth2);
        LineCharts.fillSubsYearChart(subsLastYear2);
        LineCharts.fillSubsEverChart(subsEver2);
        AreaCharts.fillCostWeekChart(costLastWeek);
        AreaCharts.fillCostMonthChart(costLastMonth);
        AreaCharts.fillCostYearChart(costLastYear);
        AreaCharts.fillCostEverChart(costEver);
        BarCharts.fillBestCategoryWeekChart(bestCategoryChartWeek);
        BarCharts.fillBestCategoryMonthChart(bestCategoryChartMonth);
        BarCharts.fillBestCategoryYearChart(bestCategoryChartYear);
        BarCharts.fillBestCategoryEverChart(bestCategoryChartEver);
    }

    /**
     * Fill in the charts to be displayed in the users tab
     */
    private void fillUserCharts() {
        PieCharts.fillSexChart(sexChart);
        PieCharts.fillRegionChart(regionChart);
        BarCharts.fillAgeChart(ageChart);
        fillTimeUserCharts();
    }

    /**
     * Fill in the time charts to be displayed in the users tab
     */
    private void fillTimeUserCharts() {
        BarCharts.fillSubsWeekChart(subsLastWeek);
        BarCharts.fillSubsMonthChart(subsLastMonth);
        BarCharts.fillSubsYearChart(subsLastYear);
        BarCharts.fillSubsEverChart(subsEver);
        BarCharts.fillOrdersWeekChart(bestClientOrderWeek);
        BarCharts.fillOrdersMonthChart(bestClientOrderMonth);
        BarCharts.fillOrdersYearChart(bestClientOrderYear);
        BarCharts.fillOrdersEverChart(bestClientOrderEver);
        BarCharts.fillUserCostWeekChart(bestClientCostWeek);
        BarCharts.fillUserCostMonthChart(bestClientCostMonth);
        BarCharts.fillUserCostYearChart(bestClientCostYear);
        BarCharts.fillUserCostEverChart(bestClientCostEver);
    }

    /**
     * Fill in the charts to be displayed in the products tab
     */
    private void fillProductCharts() {
        PieCharts.fillBestCategoryChart(bestCategoryChart);
        BarCharts.fillCategoryChart(categoryChart2);
        LineCharts.fillSalesTimeChart(salesThroughTime2);
        BarCharts.filltop10ProductsChart(top10Products2);
        fillTimeProductCharts();
    }

    /**
     * Fill in the time charts to be displayed in the products tab
     */
    private void fillTimeProductCharts() {
        AreaCharts.fillCostWeekChart(costLastWeek2);
        AreaCharts.fillCostMonthChart(costLastMonth2);
        AreaCharts.fillCostYearChart(costLastYear2);
        AreaCharts.fillCostEverChart(costEver2);
    }

    /**
     * View the charts of the general tab
     */
    public void viewGeneralScrollPane() {
        userScrollPane.setDisable(true);
        userScrollPane.setVisible(false);
        productScrollPane.setDisable(true);
        productScrollPane.setVisible(false);
        selectedProductScrollPane.setDisable(true);
        selectedProductScrollPane.setVisible(false);
        Platform.runLater(() -> fillGeneralCharts());
        generalScrollPane.setDisable(false);
        generalScrollPane.setVisible(true);
    }

    /**
     * View the charts of the user tab
     */
    public void viewUserScrollPane() {
        generalScrollPane.setDisable(true);
        generalScrollPane.setVisible(false);
        productScrollPane.setDisable(true);
        productScrollPane.setVisible(false);
        selectedProductScrollPane.setDisable(true);
        selectedProductScrollPane.setVisible(false);
        Platform.runLater(() -> fillUserCharts());
        userScrollPane.setDisable(false);
        userScrollPane.setVisible(true);
    }

    /**
     * View the charts of the product tab
     */
    public void viewProductScrollPane() {
        generalScrollPane.setDisable(true);
        generalScrollPane.setVisible(false);
        userScrollPane.setDisable(true);
        userScrollPane.setVisible(false);
        selectedProductScrollPane.setDisable(true);
        selectedProductScrollPane.setVisible(false);
        Platform.runLater(() -> fillProductCharts());
        productScrollPane.setDisable(false);
        productScrollPane.setVisible(true);
    }
}