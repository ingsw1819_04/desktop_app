package Window.Controller.Charts;

import Utilities.AlertUtility;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static Utilities.ChoiceUtility.choiceTimeRange;
import static Utilities.Constants.CONNECTION;
import static Utilities.Constants.dataSeries;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class LineCharts {

    /**
     * Fills the sales chart
     *
     * @param salesTimeChart allows display in the fxml file
     */
    public static void fillSalesTimeChart(LineChart salesTimeChart) {
        LOGGER.info(LOGGER.getName() + ".fillSalesTimeChart()");
        fillChart(salesTimeChart, "SELECT DATE, QTY FROM SALES_TIME", 0);
    }

    /**
     * Fills the subscriptions chart for the week
     *
     * @param salesWeekChart allows display in the fxml file
     */
    public static void fillSubsWeekChart(LineChart salesWeekChart) {
        LOGGER.info(LOGGER.getName() + ".fillSubsWeekChart()");
        fillChart(salesWeekChart, "SELECT DATE(REGISTER_DATE) AS DATE, COUNT(REGISTER_DATE) AS SUBS "
                + "FROM Users WHERE DATEDIFF(NOW(), REGISTER_DATE) <= 7 GROUP BY DATE ORDER BY DATE", 1);
    }

    /**
     * Fills the subscriptions chart for the month
     *
     * @param salesMonthChart allows display in the fxml file
     */
    public static void fillSubsMonthChart(LineChart salesMonthChart) {
        LOGGER.info(LOGGER.getName() + ".fillSubsMonthChart()");
        fillChart(salesMonthChart, "SELECT DATE(REGISTER_DATE) AS DATE, COUNT(REGISTER_DATE) AS SUBS "
                + "FROM Users WHERE DATEDIFF(NOW(), REGISTER_DATE) <= 30 GROUP BY DATE ORDER BY DATE", 2);
    }

    /**
     * Fills the sales chart for the year
     *
     * @param salesYearChart allows display in the fxml file
     */
    public static void fillSubsYearChart(LineChart salesYearChart) {
        LOGGER.info(LOGGER.getName() + ".fillSubsYearChart()");
        fillChart(salesYearChart, "SELECT DATE(REGISTER_DATE) AS DATE, COUNT(REGISTER_DATE) AS SUBS "
                + "FROM Users WHERE DATEDIFF(NOW(), REGISTER_DATE) <= 365 GROUP BY DATE ORDER BY DATE", 3);
    }

    /**
     * Fills the sales chart for ever
     *
     * @param salesEverChart allows display in the fxml file
     */
    public static void fillSubsEverChart(LineChart salesEverChart) {
        LOGGER.info(LOGGER.getName() + ".fillSubsEverChart()");
        fillChart(salesEverChart, "SELECT DATE(REGISTER_DATE) AS DATE, COUNT(REGISTER_DATE) AS SUBS "
                + "FROM Users GROUP BY DATE ORDER BY DATE", 4);
    }

    /**
     * Fills the cost chart for the week
     *
     * @param costWeekChart allows display in the fxml file
     */
    public static void fillCostWeekChart(LineChart costWeekChart) {
        LOGGER.info(LOGGER.getName() + ".fillCostWeekChart()");
        fillCostChart(costWeekChart, "SELECT DATE(DATE_CREATED) AS DATE, SUM(TOTAL) AS COST, AVG(TOTAL) AS AVG "
                + "FROM Orders WHERE DATEDIFF(NOW(), DATE_CREATED) <= 7 GROUP BY DATE", 0);
    }

    /**
     * Fills the cost chart for the month
     *
     * @param costMonthChart allows display in the fxml file
     */
    public static void fillCostMonthChart(LineChart costMonthChart) {
        LOGGER.info(LOGGER.getName() + ".fillCostMonthChart()");
        fillCostChart(costMonthChart, "SELECT DATE(DATE_CREATED) AS DATE, SUM(TOTAL) AS COST, AVG(TOTAL) AS AVG "
                + "FROM Orders WHERE DATEDIFF(NOW(), DATE_CREATED) <= 30 GROUP BY DATE", 1);
    }

    /**
     * Fills the cost chart for the year
     *
     * @param costYearChart allows display in the fxml file
     */
    public static void fillCostYearChart(LineChart costYearChart) {
        LOGGER.info(LOGGER.getName() + ".fillCostYearChart()");
        fillCostChart(costYearChart, "SELECT DATE(DATE_CREATED) AS DATE, SUM(TOTAL) AS COST, AVG(TOTAL) AS AVG "
                + "FROM Orders WHERE DATEDIFF(NOW(), DATE_CREATED) <= 365 GROUP BY DATE", 2);
    }

    /**
     * Fills the cost chart for ever
     *
     * @param costEverChart allows display in the fxml file
     */
    public static void fillCostEverChart(LineChart costEverChart) {
        LOGGER.info(LOGGER.getName() + ".fillCostEverChart()");
        fillCostChart(costEverChart, "SELECT DATE(DATE_CREATED) AS DATE, SUM(TOTAL) AS COST, AVG(TOTAL) AS AVG "
                + "FROM Orders GROUP BY DATE", 3);
    }

    /**
     * Fills the sale chart of the selected product
     *
     * @param selProdSalesChart allows display in the fxml file
     * @param productName       name of the product to which the chart refers
     */
    public static void fillSelectedProductSalesTime(LineChart selProdSalesChart, String productName) {
        LOGGER.info(LOGGER.getName() + ".fillSelectedProductSalesTime()");
        selProdSalesChart.setTitle("Vendite del prodotto");
        fillSelectedProductChart(selProdSalesChart, "SELECT DATE(Orders.DATE_CREATED) AS DATE, Order_Details.QUANTITY AS QTY\n" +
                "FROM Order_Details JOIN Orders ON Order_Details.ORDER_ID = Orders.ID\n" +
                "WHERE Order_Details.PRODUCT_NAME LIKE ?\n" +
                "ORDER BY DATE", productName);
    }

    /**
     * Fills the cost chart of the selected product
     *
     * @param selProdCostChart allows display in the fxml file
     * @param productName      name of the product to which the chart refers
     */
    public static void fillSelectedProductCostTime(LineChart selProdCostChart, String productName) {
        LOGGER.info(LOGGER.getName() + ".fillSelectedProductCostTime()");
        selProdCostChart.setTitle("Costo del prodotto");
        fillSelectedProductChart(selProdCostChart, "SELECT DATE(For_Product_Stats.DATE) AS DATE, For_Product_Stats.COST AS PRICE\n" +
                "FROM For_Product_Stats JOIN Products ON For_Product_Stats.PRODUCT_ID = Products.ID\n" +
                "WHERE Products.NAME LIKE ?\n" +
                "ORDER BY DATE", productName);
    }

    /**
     * Fills the selected product charts
     *
     * @param chart       to fill
     * @param query       request to be made to the database
     * @param productName name of the product to which the chart refers
     */
    private static void fillSelectedProductChart(LineChart chart, String query, String productName) {
        LOGGER.info(LOGGER.getName() + ".fillSelectedProductChart()");
        dataSeries = new XYChart.Series[1];
        if (chart.getData() != null)
            chart.getData().clear();
        try {
            LOGGER.info("Attempt to the connection to the database");
            PreparedStatement stmt = CONNECTION.prepareStatement(query);
            stmt.setString(1, "%" + productName.replace(' ', '%') + "%");
            ResultSet rs = stmt.executeQuery();
            dataSeries[0] = new XYChart.Series();
            dataSeries[0].setName(productName);
            while (rs.next()) {
                dataSeries[0].getData().add(new XYChart.Data(rs.getString(1), rs.getDouble(2)));
            }
            chart.getData().add(dataSeries[0]);
            LOGGER.info("Data successfully recovered");
        } catch (SQLException | NullPointerException ex) {
            LOGGER.severe("Error with the connection to the database");
            AlertUtility.displayErrorAlert("Errore con la connessione al DataBase");
        }
    }

    /**
     * Fills the charts
     *
     * @param chart  to fill
     * @param query  request to be made to the database
     * @param choice time frame considered
     */
    private static void fillChart(LineChart chart, String query, int choice) {
        LOGGER.info(LOGGER.getName() + ".fillChart()");
        dataSeries = new XYChart.Series[1];
        if (chart.getData() != null)
            chart.getData().clear();
        try {
            LOGGER.info("Attempt to the connection to the database");
            Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            dataSeries[0] = new XYChart.Series();
            switch (choice) {
                case 0:
                    dataSeries[0].setName("Vendite nel tempo");
                    break;
                case 1:
                    dataSeries[0].setName("Iscrizioni ultima settimana");
                    break;
                case 2:
                    dataSeries[0].setName("Iscrizioni ultimo mese");
                    break;
                case 3:
                    dataSeries[0].setName("Iscrizioni ultimo anno");
                    break;
                case 4:
                    dataSeries[0].setName("Iscrizioni da sempre");
                    break;
            }
            while (resultSet.next()) {
                dataSeries[0].getData().add(new XYChart.Data(resultSet.getString(1), resultSet.getInt(2)));
            }
            chart.getData().add(dataSeries[0]);
            LOGGER.info("Data successfully recovered");
        } catch (SQLException | NullPointerException ex) {
            LOGGER.severe("Error with the connection to the database");
            AlertUtility.displayErrorAlert("Errore con la connessione al DataBase");
        }
    }

    /**
     * Fills the cost chart
     *
     * @param lineChart to fill
     * @param query     request to be made to the database
     * @param choice    time frame considered
     */
    private static void fillCostChart(LineChart lineChart, String query, int choice) {
        LOGGER.info(LOGGER.getName() + ".fillCostChart()");
        dataSeries = new XYChart.Series[2];
        if (lineChart.getData() != null)
            lineChart.getData().clear();
        try {
            LOGGER.info("Attempt to the connection to the database");
            Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            dataSeries[0] = new XYChart.Series();
            dataSeries[1] = new XYChart.Series();
            dataSeries[1].setName("Valore Medio");
            choiceTimeRange(choice);
            while (resultSet.next()) {
                dataSeries[0].getData().add(new XYChart.Data(resultSet.getString(1), resultSet.getDouble(2)));
                dataSeries[1].getData().add(new XYChart.Data(resultSet.getString(1), resultSet.getDouble(3)));
            }
            lineChart.getData().add(dataSeries);
            LOGGER.info("Data successfully recovered");
        } catch (SQLException | NullPointerException ex) {
            LOGGER.severe("Error with the connection to the database");
            AlertUtility.displayErrorAlert("Errore con la connessione al DataBase");
        }
    }
}
