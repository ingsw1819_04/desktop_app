package Window.Controller.Charts;

import Utilities.AlertUtility;
import javafx.scene.chart.PieChart;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static Utilities.Constants.CONNECTION;
import static Utilities.Constants.arrSlices;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class PieCharts {

    /**
     * Fills the sex chart
     *
     * @param sexChart allows display in the fxml file
     */
    public static void fillSexChart(PieChart sexChart) {
        LOGGER.info(LOGGER.getName() + ".fillSexChart()");
        fillChart(sexChart, "SELECT Percentage FROM SEX_PERCENTAGE", true);
    }

    /**
     * Fills the region chart
     *
     * @param regionChart allows display in the fxml file
     */
    public static void fillRegionChart(PieChart regionChart) {
        LOGGER.info(LOGGER.getName() + ".fillRegionChart()");
        fillChart(regionChart, "SELECT REGION, PERCENTAGE FROM REGION_PERCENTAGE", false);
    }

    /**
     * Fills the best category chart
     *
     * @param bestCategoryChart allows display in the fxml file
     */
    public static void fillBestCategoryChart(PieChart bestCategoryChart) {
        LOGGER.info(LOGGER.getName() + ".fillBestCategoryChart()");
        fillChart(bestCategoryChart, "SELECT CATEGORY, PROD_PERC FROM BEST_CATEGORIES LIMIT 20", false);
    }

    /**
     * Fills the charts
     *
     * @param chart  to fill
     * @param query  request to be made to the database
     * @param isSexChart for sexChart
     */
    private static void fillChart(PieChart chart, String query, boolean isSexChart) {
        LOGGER.info(LOGGER.getName() + ".fillChart()");
        if (isSexChart) {
            arrSlices = new PieChart.Data[2];
            arrSlices[0] = new PieChart.Data("Female", -1.0);
            arrSlices[1] = new PieChart.Data("Male", -1.0);
        } else {
            arrSlices = new PieChart.Data[20];
        }
        if (chart.getData() != null)
            chart.getData().clear();
        try {
            LOGGER.info("Attempt to the connection to the database");
            Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            int i = 0;
            while (resultSet.next()) {
                if (isSexChart)
                    arrSlices[i].setPieValue(resultSet.getDouble(1));
                else
                    arrSlices[i] = new PieChart.Data(resultSet.getString(1), resultSet.getDouble(2));
                chart.getData().add(arrSlices[i++]);
            }
            LOGGER.info("Data successfully recovered");
        } catch (SQLException | NullPointerException ex) {
            LOGGER.severe("Error with the connection to the database");
            AlertUtility.displayErrorAlert("Errore con la connessione al DataBase");
        }
    }

}
