package Window.Controller.Charts;

import Utilities.AlertUtility;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.XYChart;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static Utilities.ChoiceUtility.choiceTimeRange;
import static Utilities.Constants.CONNECTION;
import static Utilities.Constants.dataSeries;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;


public class AreaCharts {

    /**
     * Fills the cost chart for the last week
     *
     * @param costWeekChart allows display in the fxml file
     */
    public static void fillCostWeekChart(AreaChart costWeekChart) {
        LOGGER.info(LOGGER.getName() + ".fillCostWeekChart()");
        fillCostChart(costWeekChart, "SELECT DATE(DATE_CREATED) AS DATE, SUM(TOTAL) AS COST, AVG(TOTAL) AS AVG "
                + "FROM Orders WHERE DATEDIFF(NOW(), DATE_CREATED) <= 7 GROUP BY DATE", 0);
    }

    /**
     * Fills the cost chart for the last month
     *
     * @param costMonthChart allows display in the fxml file
     */
    public static void fillCostMonthChart(AreaChart costMonthChart) {
        LOGGER.info(LOGGER.getName() + ".fillCostMonthChart()");
        fillCostChart(costMonthChart, "SELECT DATE(DATE_CREATED) AS DATE, SUM(TOTAL) AS COST, AVG(TOTAL) AS AVG "
                + "FROM Orders WHERE DATEDIFF(NOW(), DATE_CREATED) <= 30 GROUP BY DATE", 1);
    }

    /**
     * Fills the cost chart for the last year
     *
     * @param costYearChart allows display in the fxml file
     */
    public static void fillCostYearChart(AreaChart costYearChart) {
        LOGGER.info(LOGGER.getName() + ".fillCostYearChart()");
        fillCostChart(costYearChart, "SELECT DATE(DATE_CREATED) AS DATE, SUM(TOTAL) AS COST, AVG(TOTAL) AS AVG "
                + "FROM Orders WHERE DATEDIFF(NOW(), DATE_CREATED) <= 365 GROUP BY DATE", 2);
    }

    /**
     * Fills the cost chart for ever
     *
     * @param costEverChart allows display in the fxml file
     */
    public static void fillCostEverChart(AreaChart costEverChart) {
        LOGGER.info(LOGGER.getName() + ".fillCostEverChart()");
        fillCostChart(costEverChart, "SELECT DATE(DATE_CREATED) AS DATE, SUM(TOTAL) AS COST, AVG(TOTAL) AS AVG "
                + "FROM Orders GROUP BY DATE", 3);
    }

    /**
     * Fills the cost chart
     *
     * @param chart  to fill
     * @param query  request to be made to the database
     * @param choice time range of the chart
     */
    private static void fillCostChart(AreaChart chart, String query, int choice) {
        dataSeries = new XYChart.Series[2];
        LOGGER.info(LOGGER.getName() + ".fillCostChart()");
        if (chart.getData() != null)
            chart.getData().clear();
        try {
            LOGGER.info("Attempt to the connection to the database");
            Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            dataSeries[0] = new XYChart.Series();
            dataSeries[1] = new XYChart.Series();
            dataSeries[1].setName("Valore Medio");
            choiceTimeRange(choice);
            while (resultSet.next()) {
                dataSeries[0].getData().add(new XYChart.Data(resultSet.getString(1), resultSet.getDouble(2)));
                dataSeries[1].getData().add(new XYChart.Data(resultSet.getString(1), resultSet.getDouble(3)));
            }
            chart.getData().addAll(dataSeries[0], dataSeries[1]);
            LOGGER.info("Data successfully recovered");
        } catch (SQLException | NullPointerException ex) {
            LOGGER.severe("Error with the connection to the database");
            AlertUtility.displayErrorAlert("Errore con la connessione al DataBase");
        }
    }
}
