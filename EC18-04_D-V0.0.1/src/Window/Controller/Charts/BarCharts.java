package Window.Controller.Charts;

import Utilities.AlertUtility;
import Utilities.NameShortner;
import javafx.application.Platform;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.XYChart;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static Utilities.Constants.*;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class BarCharts {

    /**
     * Fills the age chart
     *
     * @param ageChart allows display in the fxml file
     */
    public static void fillAgeChart(BarChart ageChart) {
        LOGGER.info(LOGGER.getName() + ".fillAgeChart()");
        fillChart(ageChart, "SELECT AGE_RANGE, Customers FROM AGE_COUNT", false, false);
    }

    /**
     * Fills the category chart
     *
     * @param categoryChart allows display in the fxml file
     */
    public static void fillCategoryChart(BarChart categoryChart) {
        LOGGER.info(LOGGER.getName() + ".fillCategoryChart()");
        fillChart(categoryChart, "SELECT CONCAT(CATEGORY, \" - \", NAME), TOT FROM BEST_PROD_IN_CAT LIMIT 3", true, true);
    }

    /**
     * Fills the top 10 chart
     *
     * @param top10Chart allows display in the fxml file
     */
    public static void filltop10ProductsChart(BarChart top10Chart) {
        LOGGER.info(LOGGER.getName() + ".filltop10ProductsChart()");
        fillChart(top10Chart, "SELECT NAME, TOT FROM SOLD_PRODUCTS ORDER BY TOT DESC LIMIT 10", true, false);
    }

    /**
     * Fills the subscriptions chart for the last week
     *
     * @param subsLastWeek allows display in the fxml file
     */
    public static void fillSubsWeekChart(BarChart subsLastWeek) {
        LOGGER.info(LOGGER.getName() + ".fillSubsWeekChart()");
        Platform.runLater(() -> setMaxBarWidth(subsLastWeek, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillTimeChart(subsLastWeek, "SELECT COUNT(REGISTER_DATE) AS SUBS FROM Users WHERE DATEDIFF(NOW(), REGISTER_DATE) <= 7", 0);
    }

    /**
     * Fills the subscriptions chart for the last month
     *
     * @param subsLastMonth allows display in the fxml file
     */
    public static void fillSubsMonthChart(BarChart subsLastMonth) {
        LOGGER.info(LOGGER.getName() + ".fillSubsWeekChart()");
        Platform.runLater(() -> setMaxBarWidth(subsLastMonth, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillTimeChart(subsLastMonth, "SELECT COUNT(REGISTER_DATE) AS SUBS FROM Users WHERE DATEDIFF(NOW(), REGISTER_DATE) <= 30", 1);
    }

    /**
     * Fills the subscriptions chart for the last year
     *
     * @param subsLastYear allows display in the fxml file
     */
    public static void fillSubsYearChart(BarChart subsLastYear) {
        LOGGER.info(LOGGER.getName() + ".fillSubsWeekChart()");
        Platform.runLater(() -> setMaxBarWidth(subsLastYear, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillTimeChart(subsLastYear, "SELECT COUNT(REGISTER_DATE) AS SUBS FROM Users WHERE DATEDIFF(NOW(), REGISTER_DATE) <= 365", 2);
    }

    /**
     * Fills the subscriptions chart for ever
     *
     * @param subsEver allows display in the fxml file
     */
    public static void fillSubsEverChart(BarChart subsEver) {
        LOGGER.info(LOGGER.getName() + ".fillSubsWeekChart()");
        Platform.runLater(() -> setMaxBarWidth(subsEver, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillTimeChart(subsEver, "SELECT COUNT(REGISTER_DATE) AS SUBS FROM Users", 3);
    }

    /**
     * Fills the order chart for the last week
     *
     * @param ordersLastWeek allows display in the fxml file
     */
    public static void fillOrdersWeekChart(BarChart ordersLastWeek) {
        LOGGER.info(LOGGER.getName() + ".fillOrdersWeekChart()");
        Platform.runLater(() -> setMaxBarWidth(ordersLastWeek, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillBestTimeChart(ordersLastWeek, "SELECT Users.USERNAME AS NICKNAME, SUM(Order_Details.QUANTITY) AS NUM_ORDERS " + "FROM Orders JOIN Users ON Orders.USER_ID = Users.ID JOIN Order_Details ON Orders.ID = Order_Details.ORDER_ID " + "WHERE DATEDIFF(NOW(), Orders.DATE_CREATED) <= 7 GROUP BY NICKNAME ORDER BY NUM_ORDERS DESC, NICKNAME LIMIT 3", 0);
    }

    /**
     * Fills the order chart for the last month
     *
     * @param ordersLastMonth allows display in the fxml file
     */
    public static void fillOrdersMonthChart(BarChart ordersLastMonth) {
        LOGGER.info(LOGGER.getName() + ".fillOrdersMonthChart()");
        Platform.runLater(() -> setMaxBarWidth(ordersLastMonth, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillBestTimeChart(ordersLastMonth, "SELECT Users.USERNAME AS NICKNAME, SUM(Order_Details.QUANTITY) AS NUM_ORDERS " + "FROM Orders JOIN Users ON Orders.USER_ID = Users.ID JOIN Order_Details ON Orders.ID = Order_Details.ORDER_ID " + "WHERE DATEDIFF(NOW(), Orders.DATE_CREATED) <= 30 GROUP BY NICKNAME ORDER BY NUM_ORDERS DESC, NICKNAME LIMIT 3", 1);
    }

    /**
     * Fills the order chart for the last year
     *
     * @param ordersLastYear allows display in the fxml file
     */
    public static void fillOrdersYearChart(BarChart ordersLastYear) {
        LOGGER.info(LOGGER.getName() + ".fillOrdersYearChart()");
        Platform.runLater(() -> setMaxBarWidth(ordersLastYear, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillBestTimeChart(ordersLastYear, "SELECT Users.USERNAME AS NICKNAME, SUM(Order_Details.QUANTITY) AS NUM_ORDERS " + "FROM Orders JOIN Users ON Orders.USER_ID = Users.ID JOIN Order_Details ON Orders.ID = Order_Details.ORDER_ID " + "WHERE DATEDIFF(NOW(), Orders.DATE_CREATED) <= 365 GROUP BY NICKNAME ORDER BY NUM_ORDERS DESC, NICKNAME LIMIT 3", 2);
    }

    /**
     * Fills the order chart for ever
     *
     * @param ordersEver allows display in the fxml file
     */
    public static void fillOrdersEverChart(BarChart ordersEver) {
        LOGGER.info(LOGGER.getName() + ".fillOrdersEverChart()");
        Platform.runLater(() -> setMaxBarWidth(ordersEver, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillBestTimeChart(ordersEver, "SELECT Users.USERNAME AS NICKNAME, SUM(Order_Details.QUANTITY) AS NUM_ORDERS " + "FROM Orders JOIN Users ON Orders.USER_ID = Users.ID JOIN Order_Details ON Orders.ID = Order_Details.ORDER_ID " + "GROUP BY NICKNAME ORDER BY NUM_ORDERS DESC, NICKNAME LIMIT 3", 3);
    }

    /**
     * Fills the cost chart for the last week
     *
     * @param userCostLastWeek allows display in the fxml file
     */
    public static void fillUserCostWeekChart(BarChart userCostLastWeek) {
        LOGGER.info(LOGGER.getName() + ".fillUserCostWeekChart()");
        Platform.runLater(() -> setMaxBarWidth(userCostLastWeek, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillBestTimeChart(userCostLastWeek, "SELECT Users.USERNAME AS NICKNAME, SUM(Orders.TOTAL) AS TOT_ORDER " + "FROM Orders JOIN Users ON Orders.USER_ID = Users.ID " + "WHERE DATEDIFF(NOW(), Orders.DATE_CREATED) <= 7 GROUP BY NICKNAME ORDER BY TOT_ORDER DESC, NICKNAME LIMIT 3", 4);
    }

    /**
     * Fills the cost chart for the last month
     *
     * @param userCostLastMonth allows display in the fxml file
     */
    public static void fillUserCostMonthChart(BarChart userCostLastMonth) {
        LOGGER.info(LOGGER.getName() + ".fillUserCostMonthChart()");
        Platform.runLater(() -> setMaxBarWidth(userCostLastMonth, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillBestTimeChart(userCostLastMonth, "SELECT Users.USERNAME AS NICKNAME, SUM(Orders.TOTAL) AS TOT_ORDER " + "FROM Orders JOIN Users ON Orders.USER_ID = Users.ID " + "WHERE DATEDIFF(NOW(), Orders.DATE_CREATED) <= 30 GROUP BY NICKNAME ORDER BY TOT_ORDER DESC, NICKNAME LIMIT 3", 5);
    }

    /**
     * Fills the cost chart for the last year
     *
     * @param userCostLastYear allows display in the fxml file
     */
    public static void fillUserCostYearChart(BarChart userCostLastYear) {
        LOGGER.info(LOGGER.getName() + ".fillUserCostYearChart()");
        fillBestTimeChart(userCostLastYear, "SELECT Users.USERNAME AS NICKNAME, SUM(Orders.TOTAL) AS TOT_ORDER " + "FROM Orders JOIN Users ON Orders.USER_ID = Users.ID " + "WHERE DATEDIFF(NOW(), Orders.DATE_CREATED) <= 365 GROUP BY NICKNAME ORDER BY TOT_ORDER DESC, NICKNAME LIMIT 3", 6);
    }

    /**
     * Fills the cost chart for ever
     *
     * @param userCostEver allows display in the fxml file
     */
    public static void fillUserCostEverChart(BarChart userCostEver) {
        LOGGER.info(LOGGER.getName() + ".fillUserCostEverChart()");
        Platform.runLater(() -> setMaxBarWidth(userCostEver, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillBestTimeChart(userCostEver, "SELECT Users.USERNAME AS NICKNAME, SUM(Orders.TOTAL) AS TOT_ORDER " + "FROM Orders JOIN Users ON Orders.USER_ID = Users.ID " + "GROUP BY NICKNAME ORDER BY TOT_ORDER DESC, NICKNAME LIMIT 3", 7);
    }

    /**
     * Fills the category chart for the last week
     *
     * @param categoryWeek allows display in the fxml file
     */
    public static void fillBestCategoryWeekChart(BarChart categoryWeek) {
        LOGGER.info(LOGGER.getName() + ".fillBestCategoryWeekChart()");
        Platform.runLater(() -> setMaxBarWidth(categoryWeek, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillBestTimeChart(categoryWeek, "SELECT Products.CATEGORY AS CAT, SUM(Order_Details.QUANTITY) AS QTY " + "FROM Orders JOIN Order_Details ON Orders.ID = Order_Details.ORDER_ID JOIN Products ON Order_Details.PRODUCT_ID = Products.ID " + "WHERE DATEDIFF(NOW(), Orders.DATE_CREATED) <= 7 GROUP BY CAT ORDER BY QTY DESC LIMIT 3", 8);
    }

    /**
     * Fills the category chart for the last month
     *
     * @param categoryMonth allows display in the fxml file
     */
    public static void fillBestCategoryMonthChart(BarChart categoryMonth) {
        LOGGER.info(LOGGER.getName() + ".fillBestCategoryMonthChart()");
        fillBestTimeChart(categoryMonth, "SELECT Products.CATEGORY AS CAT, SUM(Order_Details.QUANTITY) AS QTY " + "FROM Orders JOIN Order_Details ON Orders.ID = Order_Details.ORDER_ID JOIN Products ON Order_Details.PRODUCT_ID = Products.ID " + "WHERE DATEDIFF(NOW(), Orders.DATE_CREATED) <= 30 GROUP BY CAT ORDER BY QTY DESC LIMIT 3", 9);
    }

    /**
     * Fills the category chart for the last year
     *
     * @param categoryYear allows display in the fxml file
     */
    public static void fillBestCategoryYearChart(BarChart categoryYear) {
        LOGGER.info(LOGGER.getName() + ".fillBestCategoryYearChart()");
        fillBestTimeChart(categoryYear, "SELECT Products.CATEGORY AS CAT, SUM(Order_Details.QUANTITY) AS QTY " + "FROM Orders JOIN Order_Details ON Orders.ID = Order_Details.ORDER_ID JOIN Products ON Order_Details.PRODUCT_ID = Products.ID " + "WHERE DATEDIFF(NOW(), Orders.DATE_CREATED) <= 365 GROUP BY CAT ORDER BY QTY DESC LIMIT 3", 10);
    }

    /**
     * Fills the category chart for ever
     *
     * @param categoryEver allows display in the fxml file
     */
    public static void fillBestCategoryEverChart(BarChart categoryEver) {
        LOGGER.info(LOGGER.getName() + ".fillBestCategoryEverChart()");
        fillBestTimeChart(categoryEver, "SELECT Products.CATEGORY AS CAT, SUM(Order_Details.QUANTITY) AS QTY " + "FROM Orders JOIN Order_Details ON Orders.ID = Order_Details.ORDER_ID JOIN Products ON Order_Details.PRODUCT_ID = Products.ID " + "GROUP BY CAT ORDER BY QTY DESC LIMIT 3", 11);
    }

    /**
     * Fills the cost chart for the selected product
     *
     * @param selProdAvailChart allows display in the fxml file
     * @param productName       name of the product to which the chart refers
     */
    public static void fillSelectedProductCostTime(BarChart selProdAvailChart, String productName) {
        LOGGER.info(LOGGER.getName() + ".fillSelectedProductCostTime()");
        Platform.runLater(() -> setMaxBarWidth(selProdAvailChart, MAX_BAR_WIDTH, MIN_CATEGORY_GAP));
        fillSelectedProductChart(selProdAvailChart, "SELECT QUANTITY FROM Products " + "WHERE NAME LIKE ?", productName);
    }

    /**
     * Set, for a given chart, the maximum width of the bars and the minimum value of the category gap
     *
     * @param barChart       chart on which to set the values
     * @param maxBarWidth    width of the bars
     * @param minCategoryGap minimum value of the category gap
     */
    private static void setMaxBarWidth(BarChart barChart, double maxBarWidth, double minCategoryGap) {
        if(barChart.getData().size() > 0) {
            CategoryAxis xAxis = (CategoryAxis) barChart.getXAxis();
            double barWidth = 0;
            do {
                double catSpace = xAxis.getCategorySpacing();
                double availableBarSpace = catSpace - (barChart.getCategoryGap() + barChart.getBarGap());
                barWidth = (availableBarSpace / barChart.getData().size()) - barChart.getBarGap();
                if (barWidth > maxBarWidth) {
                    availableBarSpace = (maxBarWidth + barChart.getBarGap()) * barChart.getData().size();
                    barChart.setCategoryGap(catSpace - availableBarSpace - barChart.getBarGap());
                }
            } while (barWidth > maxBarWidth);

            do {
                double catSpace = xAxis.getCategorySpacing();
                double availableBarSpace = catSpace - (minCategoryGap + barChart.getBarGap());
                barWidth = Math.min(maxBarWidth, (availableBarSpace / barChart.getData().size()) - barChart.getBarGap());
                availableBarSpace = (barWidth + barChart.getBarGap()) * barChart.getData().size();
                barChart.setCategoryGap(catSpace - availableBarSpace - barChart.getBarGap());
            } while (barWidth > maxBarWidth && barChart.getCategoryGap() < minCategoryGap);
        }
    }

    /**
     * Fills the selected product charts
     *
     * @param chart       to fill
     * @param query       request to be made to the database
     * @param productName name of the product to which the chart refers
     */
    private static void fillSelectedProductChart(BarChart chart, String query, String productName) {
        LOGGER.info(LOGGER.getName() + ".fillSelectedProductChart()");
        dataSeries = new XYChart.Series[1];
        if (chart.getData() != null)
            chart.getData().clear();
        try {
            LOGGER.info("Attempt to the connection to the database");
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(query);
            preparedStatement.setString(1, "%" + productName.replace(' ', '%') + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            dataSeries[0] = new XYChart.Series();
            while (resultSet.next()) {
                dataSeries[0].setName(productName);
                dataSeries[0].getData().add(new XYChart.Data("Quantità", resultSet.getDouble(1)));
            }
            chart.getData().add(dataSeries[0]);
            LOGGER.info("Data successfully recovered");
        } catch (SQLException | NullPointerException ex) {
            LOGGER.severe("Error with the connection to the database");
            AlertUtility.displayErrorAlert("Errore con la connessione al DataBase");
        }
    }

    /**
     * Fills the best time chart
     *
     * @param chart  to fill
     * @param query  request to be made to the database
     * @param choice time frame considered
     */
    private static void fillBestTimeChart(BarChart chart, String query, int choice) {
        LOGGER.info(LOGGER.getName() + ".fillBestTimeChart()");
        dataSeries = new XYChart.Series[1];
        if (chart.getData() != null)
            chart.getData().clear();
        try {
            LOGGER.info("Attempt to the connection to the database");
            Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                dataSeries[0] = new XYChart.Series();
                switch (choice) {
                    case 0:
                        dataSeries[0].setName("Miglior cliente (acquisti) - ultima settimana");
                        break;
                    case 1:
                        dataSeries[0].setName("Miglior cliente (acquisti) - ultima mese");
                        break;
                    case 2:
                        dataSeries[0].setName("Miglior cliente (acquisti) - ultima anno");
                        break;
                    case 3:
                        dataSeries[0].setName("Miglior cliente (acquisti) - da sempre");
                        break;
                    case 4:
                        dataSeries[0].setName("Miglior cliente (spesa) - ultima settimana");
                        break;
                    case 5:
                        dataSeries[0].setName("Miglior cliente (spesa) - ultimo mese");
                        break;
                    case 6:
                        dataSeries[0].setName("Miglior cliente (spesa) - ultimo anno");
                        break;
                    case 7:
                        dataSeries[0].setName("Miglior cliente (spesa) - da sempre");
                        break;
                    case 8:
                        dataSeries[0].setName("Miglior categoria - ultima settimana");
                        break;
                    case 9:
                        dataSeries[0].setName("Miglior categoria - ultimo mese");
                        break;
                    case 10:
                        dataSeries[0].setName("Miglior categoria - ultimo anno");
                        break;
                    case 11:
                        dataSeries[0].setName("Miglior categoria - da sempre");
                        break;
                }
                dataSeries[0].getData().add(new XYChart.Data(resultSet.getString(1), resultSet.getDouble(2)));
                chart.getData().add(dataSeries[0]);
            }
            LOGGER.info("Data successfully recovered");
        } catch (SQLException | NullPointerException ex) {
            LOGGER.severe("Error with the connection to the database");
            AlertUtility.displayErrorAlert("Errore con la connessione al DataBase");
        }
    }

    /**
     * Fills the time chart
     *
     * @param chart  to fill
     * @param query  request to be made to the database
     * @param choice time frame considered
     */
    private static void fillTimeChart(BarChart chart, String query, int choice) {
        LOGGER.info(LOGGER.getName() + ".fillTimeChart()");
        dataSeries = new XYChart.Series[1];
        if (chart.getData() != null)
            chart.getData().clear();
        try {
            LOGGER.info("Attempt to the connection to the database");
            Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                dataSeries[0] = new XYChart.Series();
                String label = new String();
                switch (choice) {
                    case 0:
                        dataSeries[0].setName("Iscrizioni ultima settimana");
                        label = "Ultima settimana";
                        break;
                    case 1:
                        dataSeries[0].setName("Iscrizioni ultimo mese");
                        label = "Ultimo mese";
                        break;
                    case 2:
                        dataSeries[0].setName("Iscrizioni ultimo anno");
                        label = "Ultimo anno";
                        break;
                    case 3:
                        dataSeries[0].setName("Iscrizioni da sempre");
                        label = "Da sempre";
                        break;
                    case 4:
                        dataSeries[0].setName("Miglior cliente (acquisti) - ultima settimana");
                        label = "Ultima settimana";
                        break;
                    case 5:
                        dataSeries[0].setName("Miglior cliente (acquisti) - ultima mese");
                        label = "Ultimo mese";
                        break;
                    case 6:
                        dataSeries[0].setName("Miglior cliente (acquisti) - ultima anno");
                        label = "Ultimo anno";
                        break;
                    case 7:
                        dataSeries[0].setName("Miglior cliente (acquisti) - da sempre");
                        label = "Da sempre";
                        break;
                    case 8:
                        dataSeries[0].setName("Miglior cliente (spesa) - ultima settimana");
                        label = "Ultima settimana";
                        break;
                    case 9:
                        dataSeries[0].setName("Miglior cliente (spesa) - ultimo mese");
                        label = "Ultimo mese";
                        break;
                    case 10:
                        dataSeries[0].setName("Miglior cliente (spesa) - ultimo anno");
                        label = "Ultimo anno";
                        break;
                    case 11:
                        dataSeries[0].setName("Miglior cliente (spesa) - da sempre");
                        label = "Da sempre";
                        break;
                }
                dataSeries[0].getData().add(new XYChart.Data(label, resultSet.getInt(1)));
                chart.getData().add(dataSeries[0]);
            }
            LOGGER.info("Data successfully recovered");
        } catch (SQLException | NullPointerException ex) {
            LOGGER.severe("Error with the connection to the database");
            AlertUtility.displayErrorAlert("Errore con la connessione al DataBase");
        }
    }

    /**
     * Fills the charts
     *
     * @param chart          to fill
     * @param query          request to be made to the database
     * @param nameShortening flag on the length of the product title
     * @param catShortening  flag for the shorting product title
     */
    private static void fillChart(BarChart chart, String query, boolean nameShortening, boolean catShortening) {
        LOGGER.info(LOGGER.getName() + ".fillChart()");
        dataSeries = new XYChart.Series[10];
        if (chart.getData() != null)
            chart.getData().clear();
        try {
            LOGGER.info("Attempt to the connection to the database");
            Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            int i = 0;
            while (resultSet.next()) {
                dataSeries[i] = new XYChart.Series();
                if (nameShortening)
                    if (catShortening) {
                        dataSeries[i].setName(NameShortner.catShortening(resultSet.getString(1)));
                        dataSeries[i].getData().add(new XYChart.Data(NameShortner.nameShortening(resultSet.getString(1)), resultSet.getDouble(2)));
                    } else {
                        dataSeries[i].setName(NameShortner.nameShortening(resultSet.getString(1)));
                        dataSeries[i].getData().add(new XYChart.Data(NameShortner.nameShortening(resultSet.getString(1)), resultSet.getDouble(2)));
                    }
                else {
                    dataSeries[i].setName(resultSet.getString(1));
                    dataSeries[i].getData().add(new XYChart.Data(resultSet.getString(1), resultSet.getDouble(2)));
                }
                chart.getData().add(dataSeries[i++]);
            }
            LOGGER.info("Data successfully recovered");
        } catch (SQLException | NullPointerException ex) {
            LOGGER.severe("Error with the connection to the database");
            AlertUtility.displayErrorAlert("Errore con la connessione al DataBase");
        }
    }
}
