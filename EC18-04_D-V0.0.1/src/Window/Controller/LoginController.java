package Window.Controller;

import Encoding.SHAEncryption;
import Utilities.AlertUtility;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static Utilities.Constants.CONNECTION;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class LoginController {

    @FXML
    TextField usernameField;
    @FXML
    PasswordField passwordField;
    private String username;
    private String password;
    private String matchUsername;
    private String matchPassword;

    /**
     * Calls a method for displaying the password recovery dialog
     */
    public void recoverPassword() {
        showRecoverPasswordDialog();
    }

    /**
     * Opens the password recovery dialog
     */
    private void showRecoverPasswordDialog() {
        LOGGER.info(LOGGER.getName() + ".showRecoverPasswordDialog()");
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../fxml/ChangePassword.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Cambia password");
            stage.getIcons().add(new Image("./Icon/icon.png"));
            stage.setScene(scene);
            stage.setX(1000.0);
            stage.setY(300.0);
            stage.showAndWait();
        } catch (IOException e) {
            LOGGER.warning("Something Wrong");
        }
    }

    /**
     * From the login window, if the fields are all filled in then log in, otherwise display an alert window
     */
    public void login() {
        if (!emptyFields())
            doLogin();
        else {
            AlertUtility.displayWarningAlert("Inserire tutte le credenziali");
        }
    }

    /**
     * It allows to display the statistics window
     */
    private void showStatsView() {
        LOGGER.info(LOGGER.getName() + ".showStatsView()");
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../fxml/StatsView.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Home stats");
            stage.getIcons().add(new Image("./Icon/icon.png"));
            stage.setScene(scene);
            Stage actualStage = (Stage) usernameField.getScene().getWindow();
            actualStage.close();
            stage.showAndWait();
        } catch (IOException e) {
            LOGGER.warning("Something Wrong: " + e.toString());
        }
    }

    /**
     * Check if the credentials entered in the login window. If they are correct then display the statistics window,
     * otherwise display an alert window
     */
    private void doLogin() {
        if (checkLoginCredentials()) {
            showStatsView();
        } else {
            AlertUtility.displayErrorAlert("Credenziali errate");
        }
    }

    /**
     * Check if the login fields have been entered
     *
     * @return true if at least one of the fields has not been entered, otherwise false
     */
    private boolean emptyFields() {
        return usernameField.getText().equals("") || passwordField.getText().equals("");
    }

    /**
     * Recover login credentials
     */
    private void retrieveCredentials() {
        LOGGER.info(LOGGER.getName() + ".retrieveCredentials()");
        try {
            Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Administrator");
            while (resultSet.next()) {
                username = resultSet.getString(2);
                password = resultSet.getString(3);
            }
        } catch (SQLException ex) {
            LOGGER.severe("Error in connection to the database");
            AlertUtility.displayErrorAlert("Errore con la connessione al DataBase");
        }
        matchUsername = usernameField.getText();
        matchPassword = SHAEncryption.encryptThisString(passwordField.getText());
    }

    /**
     * Check if the credentials entered are valid
     *
     * @return true if the credentials entered are valid, false otherwise
     */
    private boolean checkLoginCredentials() {
        retrieveCredentials();
        return username.equals(matchUsername) && password.equals(matchPassword);
    }
}
