package Window.Controller;

import Encoding.SHAEncryption;
import Utilities.AlertUtility;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static Utilities.Constants.CONNECTION;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class ChangePasswordController {

    @FXML
    PasswordField pass1;

    @FXML
    PasswordField pass2;

    @FXML
    Label passLabel;

    /**
     * Check if the fields are not empty and then call the method to change the password
     */
    public void saveButtonPressed() {
        LOGGER.info(LOGGER.getName() + ".saveButtonPressed()");
        if (!emptyFields()) {
            if (!passLabel.isVisible()) {
                changePasswords();
                Stage stage = (Stage) pass1.getScene().getWindow();
                stage.close();
            }
        } else {
            LOGGER.warning("Complete all fields");
            AlertUtility.displayWarningAlert("Inserire tutte le password");
        }
    }

    /**
     * Change the password on the database
     */
    private void changePasswords() {
        LOGGER.info(LOGGER.getName() + ".changePasswords()");
        String queryUpdate = "UPDATE Administrator SET USER_PASS_HASH = ? WHERE USER_USERNAME = ?";
        String newPassword = SHAEncryption.encryptThisString(pass2.getText());
        try {
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(queryUpdate);
            preparedStatement.setString(1, newPassword);
            preparedStatement.setString(2, "administrator@ecommerce18.it");
            if (preparedStatement.executeUpdate() > 0) {
                LOGGER.info("Password changed");
                AlertUtility.displayInfoAlert("Modifica effettuata con successo");
            } else {
                LOGGER.warning("Password not changed");
                AlertUtility.displayWarningAlert("Non è stato modificato alcun record");
            }
        } catch (SQLException ex) {
            LOGGER.severe("Error in saving the new password");
            AlertUtility.displayErrorAlert("Problema drante il salvataggio della nuova password");
        }
    }

    /**
     * Check if the fields are empty or not
     *
     * @return the status of the fields, empty or not
     */
    private boolean emptyFields() {
        return pass1.getText().equals("") || pass2.getText().equals("");
    }

    /**
     * Check if the two password fields match
     */
    public void matchingPasswords() {
        passLabel.setVisible(!pass1.getText().equals(pass2.getText()));
    }

}
