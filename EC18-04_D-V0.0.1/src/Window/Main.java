package Window;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class Main extends Application {

    private Stage primaryStage;

    public static void main(String[] args) {
        LOGGER.info("\n\nWelcome to \"EC18-04_d-V0.0.1\""
                + "\nAuthors: Di Lucrezia Roberto - Marino Eddy Pasquale\n");
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("fxml/Login.fxml"));
        primaryStage.setTitle("Administrator Login");
        primaryStage.getIcons().add(new Image("./Icon/icon.png"));
        primaryStage.setScene(new Scene(root, 400, 200));
        primaryStage.show();
    }
}
