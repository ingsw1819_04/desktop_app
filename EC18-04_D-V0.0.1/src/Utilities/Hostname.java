package Utilities;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class Hostname {

    /**
     * Method for resolving the hostname
     *
     * @return the resolution of the hostname, if it has been obtained
     */
    public static String getHostname() {
        LOGGER.info(LOGGER.getName() + ".getHostname()");
        String hostname = "Unknown";
        try {
            LOGGER.info("Attempt to resolve the hostname");
            InetAddress inetAddress;
            inetAddress = InetAddress.getLocalHost();
            hostname = inetAddress.getHostName();
        } catch (UnknownHostException ex) {
            LOGGER.warning("Hostname can not be resolved");
        }
        LOGGER.info("Hostname resolved as: " + hostname);
        return hostname;
    }
}
