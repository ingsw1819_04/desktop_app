package Utilities;

import static Utilities.Constants.dataSeries;

public class ChoiceUtility {

    public static void choiceTimeRange(int choice) {
        switch (choice) {
            case 0:
                dataSeries[0].setName("Valore ordini ultima settimana");
                break;
            case 1:
                dataSeries[0].setName("Valore ordini ultimo mese");
                break;
            case 2:
                dataSeries[0].setName("Valore ordini ultimo anno");
                break;
            case 3:
                dataSeries[0].setName("Valore ordini da sempre");
                break;
        }
    }
}
