package Utilities;

import Connection.DBConnection;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;

import java.sql.Connection;

public final class Constants {

    public static final int MAX_BAR_WIDTH = 150;
    public static final int MIN_CATEGORY_GAP = 10;
    public static final String URL = "jdbc:mariadb://dbprogetto.synology.me:3307/INGSW";
    public static final String URL2 = "jdbc:mariadb://192.168.1.4:3307/INGSW";
    public static final String USERNAME = "eddy";
    public static final String PASSWORD = "progettoPassword";
    public static final Connection CONNECTION = DBConnection.getConnection();
    public static PieChart.Data[] arrSlices;
    public static XYChart.Series[] dataSeries;
    private Constants() {
    }

}
