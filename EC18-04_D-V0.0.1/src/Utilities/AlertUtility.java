package Utilities;

import javafx.scene.control.Alert;

public class AlertUtility {

    /**
     * Creates an error message to be displayed on the screen as a popup
     *
     * @param contentText is the message displayed in the window
     */
    public static void displayErrorAlert(String contentText) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Errore");
        alert.setHeaderText(null);
        alert.setContentText(contentText);

        alert.showAndWait();
    }

    /**
     * Creates a warning message to be displayed on the screen as a popup
     *
     * @param contentText is the message displayed in the window
     */
    public static void displayWarningAlert(String contentText) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Attenzione");
        alert.setHeaderText(null);
        alert.setContentText(contentText);

        alert.showAndWait();
    }

    /**
     * Creates an information message to be displayed on the screen as a popup
     *
     * @param contentText is the message displayed in the window
     */
    public static void displayInfoAlert(String contentText) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informazione");
        alert.setHeaderText(null);
        alert.setContentText(contentText);

        alert.showAndWait();
    }
}
