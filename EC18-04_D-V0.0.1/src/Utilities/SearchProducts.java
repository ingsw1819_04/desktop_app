package Utilities;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import static Utilities.Constants.CONNECTION;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class SearchProducts {

    private static final String query = "SELECT NAME FROM Products WHERE NAME LIKE ? LIMIT 4";

    /**
     * Search for products on the database by filtering based on the input
     *
     * @param productFilter is the filter for database search
     * @return a list of filtered products
     */
    public static LinkedList<String> retrieveSearchedProducts(String productFilter) {
        LOGGER.info(LOGGER.getName() + ".retrieveSearchedProducts()");
        LinkedList<String> linkedList = new LinkedList<>();
        try {
            LOGGER.info("Attempt to the connection to the database");
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(query);
            preparedStatement.setString(1, "%" + productFilter + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                linkedList.add(NameShortner.searchShortening(resultSet.getString(1)));
            }
            LOGGER.info("Data successfully recovered");
        } catch (SQLException | NullPointerException ex) {
            LOGGER.severe("Error with the connection to the database");
            AlertUtility.displayErrorAlert("Errore con la connessione al DataBase");
        }
        return linkedList;
    }
}
