package Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import static Utilities.Constants.CONNECTION;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class SearchProducts2 {

    private static final String query = "SELECT NAME FROM Products";

    /**
     * Search for products on the database by filtering based on the input
     *
     * @return a list of filtered products
     */
    public static LinkedList<String> retrieveSearchedProducts() {
        LOGGER.info(LOGGER.getName() + ".retrieveSearchedProducts()");
        LinkedList<String> linkedList = new LinkedList<>();
        try {
            LOGGER.info("Attempt to the connection to the database");
            Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                linkedList.add(NameShortner.searchShortening(resultSet.getString(1)));
            }
            LOGGER.info("Data successfully recovered");
        } catch (SQLException | NullPointerException ex) {
            LOGGER.severe("Error with the connection to the database");
            AlertUtility.displayErrorAlert("Errore con la connessione al DataBase");
        }
        return linkedList;
    }
}
