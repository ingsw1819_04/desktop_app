package Utilities;

public class NameShortner {
    private static final String SHORT1 = "BoTW NS";
    private static final String SHORT2 = "EVGA RTX 2080 Ti";
    private static final String SHORT3 = "NS Nero";
    private static final String SHORT4 = "AMD 2990 Wx";
    private static final String SHORT5 = "SSBU NS";
    private static final String SHORT6 = "Sony Alpha 7M3";
    private static final String SHORT7 = "LG 55E8";
    private static final String SHORT8 = "BB Collection";
    private static final String SHORT9 = "LOTR EE";
    private static final String SHORT10 = "Macerie prime";
    private static final String SHORT11 = "GOT 1";
    private static final String SHORT12 = "Queen GH 1-2-3";
    private static final String SHORT13 = "DSOTM";
    private static final String SHORT14 = "Nike Polo";
    private static final String SHORT15 = "SEL2470GM";
    private static final String SHORT16 = "PKMN SH";
    private static final String SHORT17 = "PKMN SW";
    private static final String SHORT18 = "PKMN SWSH";
    private static final String SHORT19 = "MSI 5700 XT";
    private static final String SHORT20 = "Pulse 5700 XT";
    private static final String SHORT21 = "Fritz! 7590";
    private static final String SHORT22 = "Astral Chain";
    private static final String SHORT23 = "Astral Chain LTD";
    private static final String SHORT24 = "Cyberpunk 2077";
    private static final String SHORT25 = "Cyberpunk 2077 LTD";
    private static final String SHORT26 = "GOT 1-8";
    private static final String SHORT27 = "Luigi's Mansion 3";
    private static final String SHORT28 = "NS Lite Turchese";
    private static final String SHORT29 = "NS Lite Grigio";
    private static final String SHORT30 = "NS Lite Giallo";
    private static final String SHORT31 = "NS Lite LTD";
    private static final String SHORT32 = "Saga Geralt 1";
    private static final String SHORT33 = "Saga Geralt 2";
    private static final String SHORT34 = "Saga Geralt 3";
    private static final String SHORT35 = "Saga Geralt 4";
    private static final String SHORT36 = "Saga Geralt 5";
    private static final String SHORT37 = "Saga Geralt 6";
    private static final String SHORT38 = "Saga Geralt 7";
    private static final String SHORT39 = "Saga Geralt 8";

    private static final String SSHORT1 = "Breath of The Wild";
    private static final String SSHORT3 = "Nintendo Switch Nero";
    private static final String SSHORT5 = "Super Smash Bros Ultimate";
    private static final String SSHORT8 = "Breaking Bad Collection";
    private static final String SSHORT9 = "Lord of The Rings Trilogy";
    private static final String SSHORT11 = "Il trono di Spade 1";
    private static final String SSHORT12 = "Queen Greatest Hits";
    private static final String SSHORT13 = "Dark Side of The Moon";
    private static final String SSHORT16 = "Pokémon Scudo - Single";
    private static final String SSHORT17 = "Pokémon Spada - Single";
    private static final String SSHORT18 = "Pokémon Spada Scudo - Dual Pack";
    private static final String SSHORT21 = "AVM FRITZ!Box 7590";
    private static final String SSHORT22 = "Astral Chain - Standard";
    private static final String SSHORT23 = "Astral Chain - Collector's";
    private static final String SSHORT24 = "Cyberpunk 2077 - D1 Edition";
    private static final String SSHORT25 = "Cyberpunk 2077 - Collector's";
    private static final String SSHORT26 = "Trono di Spade Stagioni 1-8";
    private static final String SSHORT27 = "Luigi's Mansion 3";
    private static final String SSHORT28 = "Nintendo Switch Lite Turchese";
    private static final String SSHORT29 = "Nintendo Switch Lite Grigio";
    private static final String SSHORT30 = "Nintendo Switch Lite Giallo";
    private static final String SSHORT31 = "Nintendo Switch Lite Zacian & Zamazenta Edition";
    private static final String SSHORT32 = "Il guardiano degli innocenti";
    private static final String SSHORT33 = "La spada del destino";
    private static final String SSHORT34 = "Il sangue degli elfi";
    private static final String SSHORT35 = "Il tempo della guerra";
    private static final String SSHORT36 = "Il battesimo del fuoco";
    private static final String SSHORT37 = "La torre della rondine";
    private static final String SSHORT38 = "La signora del lago";
    private static final String SSHORT39 = "La stagione delle tempeste";

    public static String catShortening(String aux) {
        if (aux.contains("2080 Ti") || aux.contains("2990 Wx") || aux.contains("5700") || aux.contains("7590"))
            aux = "Informatica";
        else if (aux.contains("Nintendo Switch") || aux.contains("Breath of the Wild") || aux.contains("Super Smash") || aux.contains("Scudo") || aux.contains("Spada")
                || aux.contains("Luigi") || aux.contains("Astral") || aux.contains("2077"))
            aux = "Videogiochi";
        else if (aux.contains("Sony Alpha 7M3") || aux.contains("SEL2470GM"))
            aux = "Fotografia";
        else if (aux.contains(" LG OLED AI ThinQ 55E8"))
            aux = "Audio e Video";
        else if (aux.contains("Breaking Bad") || aux.contains("Stagioni 1-8"))
            aux = "Serie TV";
        else if (aux.contains("The Lord of the Rings"))
            aux = "Film";
        else if (aux.contains("Macerie prime") || aux.contains("Il trono di spade: 1") || aux.contains("guardiano") || aux.contains("destino") || aux.contains("sangue")
                || aux.contains("guerra") || aux.contains("battesimo") || aux.contains("rondine") || aux.contains("signora del lago") || aux.contains("tempeste"))
            aux = "Libri";
        else if (aux.contains("Queen Greatest") || aux.contains("Dark Side of the Moon"))
            aux = "Musica";
        else if (aux.contains("Nike TS Core"))
            aux = "Abbigliamento";
        return aux;
    }

    public static String searchShortening(String aux) {
        aux = getShortString(aux, true);
        return aux;
    }

    public static String nameShortening(String aux) {
        aux = getShortString(aux, false);
        return aux;
    }

    private static String getShortString(String aux, boolean search) {
        if (aux.contains("Breath of the Wild"))
            aux = search ? SSHORT1 : SHORT1;
        else if (aux.contains("2080 Ti"))
            aux = SHORT2;
        else if (aux.contains("Switch Nero"))
            aux = search ? SSHORT3 : SHORT3;
        else if (aux.contains("2990 Wx"))
            aux = SHORT4;
        else if (aux.contains("Super Smash"))
            aux = search ? SSHORT5 : SHORT5;
        else if (aux.contains("Sony Alpha 7M3"))
            aux = SHORT6;
        else if (aux.contains("LG OLED AI ThinQ 55E8"))
            aux = SHORT7;
        else if (aux.contains("Breaking Bad"))
            aux = search ? SSHORT8 : SHORT8;
        else if (aux.contains("The Lord of the Rings"))
            aux = search ? SSHORT9 : SHORT9;
        else if (aux.contains("Macerie prime"))
            aux = SHORT10;
        else if (aux.contains("Il trono di spade"))
            aux = search ? SSHORT11 : SHORT11;
        else if (aux.contains("Queen Greatest"))
            aux = search ? SSHORT12 : SHORT12;
        else if (aux.contains("Dark Side of the Moon"))
            aux = search ? SSHORT13 : SHORT13;
        else if (aux.contains("Nike TS Core"))
            aux = SHORT14;
        else if (aux.contains("SEL2470GM"))
            aux = SHORT15;
        else if (aux.contains("Dual Pack"))
            aux = search ? SSHORT18 : SHORT18;
        else if (aux.contains("Scudo"))
            aux = search ? SSHORT16 : SHORT16;
        else if (aux.contains("Spada"))
            aux = search ? SSHORT17 : SHORT17;
        else if (aux.contains("MSI Radeon 5700 XT"))
            aux = SHORT19;
        else if (aux.contains("Pulse Radeon RX 5700 XT"))
            aux = SHORT20;
        else if (aux.contains("7590"))
            aux = search ? SSHORT21 : SHORT21;
        else if (aux.contains("Astral Chain - Standard"))
            aux = search ? SSHORT22 : SHORT22;
        else if (aux.contains("Astral Chain - Collector’s"))
            aux = search ? SSHORT23 : SHORT23;
        else if (aux.contains("Cyberpunk 2077 - D1 Edition"))
            aux = search ? SSHORT24 : SHORT24;
        else if (aux.contains("Cyberpunk 2077 - Collector's"))
            aux = search ? SSHORT25 : SHORT25;
        else if (aux.contains("Stagioni 1-8"))
            aux = search ? SSHORT26 : SHORT26;
        else if (aux.contains("Luigi"))
            aux = search ? SSHORT27 : SHORT27;
        else if (aux.contains("Lite Turchese"))
            aux = search ? SSHORT28 : SHORT28;
        else if (aux.contains("Lite Grigio"))
            aux = search ? SSHORT29 : SHORT29;
        else if (aux.contains("Lite Giallo"))
            aux = search ? SSHORT30 : SHORT30;
        else if (aux.contains("Zamazenta"))
            aux = search ? SSHORT31 : SHORT31;
        else if (aux.contains("guardiano"))
            aux = search ? SSHORT32 : SHORT32;
        else if (aux.contains("destino"))
            aux = search ? SSHORT33 : SHORT33;
        else if (aux.contains("sangue"))
            aux = search ? SSHORT34 : SHORT34;
        else if (aux.contains("guerra"))
            aux = search ? SSHORT35 : SHORT35;
        else if (aux.contains("battesimo"))
            aux = search ? SSHORT36 : SHORT36;
        else if (aux.contains("rondine"))
            aux = search ? SSHORT37 : SHORT37;
        else if (aux.contains("signora del lago"))
            aux = search ? SSHORT38 : SHORT38;
        else
            aux = search ? SSHORT39 : SHORT39;
        return aux;
    }
}
