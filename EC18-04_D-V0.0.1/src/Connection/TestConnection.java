package Connection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestConnection {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        DBConnection dbconn = DBConnection.getInstance();
        Connection conn = DBConnection.getConnection();

        //TEST DI UNA QUERY
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Shipping_Info");
            while (rs.next()) {
                String type = rs.getString(2);
                double price = rs.getDouble(3);
                String region = rs.getString(4);
                System.out.println(type + " " + price + " " + region + "\n");
            }
        } catch (SQLException ex) {
        }
    }
}
