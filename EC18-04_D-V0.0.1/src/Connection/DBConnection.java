package Connection;

import Utilities.AlertUtility;
import Utilities.Hostname;
import com.sun.xml.internal.messaging.saaj.util.Base64;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static Utilities.Constants.*;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class DBConnection {

    private static DBConnection instance;
    private static Connection connection = null;

    private DBConnection() {
    }

    /**
     * This method allows establishing a connection with the database
     *
     * @return connection status
     */
    public static Connection getConnection() {
        LOGGER.info(LOGGER.getName() + ".getConnection()");
        try {
            LOGGER.info("Attempt to connect to the database");
            if (Hostname.getHostname().equals(Base64.base64Decode("REVTS1RPUC1NMzlGUkxQ"))) {
                connection = DriverManager.getConnection(URL2, USERNAME, PASSWORD);
            } else {
                connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            }
        } catch (SQLException e) {
            LOGGER.severe("Error in connection to the database");
            AlertUtility.displayErrorAlert("Si è verificato un problema con la connessione al DataBase");
            System.exit(1);
        }
        LOGGER.info("Successful connection");
        return connection;
    }

    /**
     * @return the connection instance
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static DBConnection getInstance() throws SQLException, ClassNotFoundException {
        if (instance == null) {
            instance = new DBConnection();
        } else if (instance.getConnection().isClosed()) {
            instance = new DBConnection();
        }
        return instance;
    }

}
