package Test.java;

import Utilities.NameShortner;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.LinkedList;

import static Utilities.Constants.CONNECTION;
import static org.junit.Assert.*;

public class SearchProductsTest {

    @Test
    public void retrieveSearchedProducts() {
        LinkedList<String> linkedList = new LinkedList<>();
        assertNotNull(CONNECTION);
        try {
            PreparedStatement preparedStatement = CONNECTION.prepareStatement("SELECT NAME FROM Products WHERE NAME LIKE ? LIMIT 4");
            assertNotNull(preparedStatement);
            preparedStatement.setString(1, "%" + "Zelda" + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            assertNotNull(resultSet);
            while (resultSet.next()) {
                linkedList.add(NameShortner.searchShortening(resultSet.getString(1)));
            }
        } catch (SQLException | NullPointerException ex) {
            assertFalse(true);
        }
        assertNotNull(linkedList);
    }
}