package Test.java;

import Utilities.NameShortner;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import static Utilities.Constants.CONNECTION;
import static org.junit.Assert.*;

public class SearchProducts2Test {

    @Test
    public void retrieveSearchedProducts() {
        LinkedList<String> linkedList = new LinkedList<>();
        assertNotNull(CONNECTION);
        try {
            Statement statement = CONNECTION.createStatement();
            assertNotNull(statement);
            ResultSet resultSet = statement.executeQuery("SELECT NAME FROM Products");
            assertNotNull(resultSet);
            while (resultSet.next()) {
                linkedList.add(NameShortner.searchShortening(resultSet.getString(1)));
            }
        } catch (SQLException | NullPointerException ex) {
            assertFalse(true);
        }
        assertNotNull(linkedList);
    }
}